# Kvizo App

Studying a new language, technology any knowledge with a quiz. 
You can add new questions, edit existing ones.
Supported libraries of questions in simple markdown format.

## Minimal requirements

* Java 8
* JavaFX

## Installation

Code by Intellij IDEA 2019 in Windows 10.
JRE 1.8.0_150

## Testing 

* JUnit 5

## Sources

* [Quiz idea](https://github.com/HouariZegai/JavaQuiz/)  
* [Markdown parser](https://github.com/leeyaf/java-markdown-parser)  
* [Timer](https://codereview.stackexchange.com/questions/59784/integer-seconds-to-formated-string-mmss)
* [Economy](https://www.councilforeconed.org/news-information/economic-literacy-quiz/)
