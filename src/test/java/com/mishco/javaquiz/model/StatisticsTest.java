package com.mishco.javaquiz.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StatisticsTest {

    private Statistics statistics;

    @BeforeEach
    void setupVariable() {
        statistics = new Statistics(null);
    }

    @Test
    void finishingResult() {
        assertEquals(0,statistics.getNumberOfCorrectAnswer());
        assertEquals(0,statistics.getNumberOfFalseAnswer());
    }

    @Test
    void testGoodParameterInput() {
        statistics = new Statistics(Parameter.getInstance());

        statistics.addCorrectAnswer();
        assertEquals(1,statistics.getNumberOfCorrectAnswer());

        statistics.addFalseAnswer();
        statistics.addCorrectAnswer();
        assertEquals(2,statistics.getNumberOfCorrectAnswer());
        assertEquals(1,statistics.getNumberOfFalseAnswer());
    }
}