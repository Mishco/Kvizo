package com.mishco.javaquiz.controller;

import com.mishco.javaquiz.model.Parameter;
import com.mishco.javaquiz.model.QuestionAndAnswer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuizMakerTest {

    @Test
    void createQuestion() {
        QuizMaker quizMaker = new QuizMaker(null);
        assertNull(quizMaker.createQuestion());
    }


    @Test
    void testRandomPickedQuestion() {
        Parameter.setInstance(QUESTION_THEME.OCA, 10,10,10);
        QuizMaker quizMaker = new QuizMaker(Parameter.getInstance());

        QuestionAndAnswer qaa1 = quizMaker.createQuestion();

        Parameter.setInstance(QUESTION_THEME.OCA, 10,10,10);
        QuizMaker quizMakerAnother = new QuizMaker(Parameter.getInstance());

        QuestionAndAnswer qaa2 = quizMakerAnother.createQuestion();

        assertNotEquals(qaa1.getPossibilities(), qaa2.getPossibilities());
    }

}