package com.mishco.javaquiz.parser;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UtilTest {

    @Test
    void isEmoji() {
        char c = 43;
        assertTrue(Util.isEmoji(c));

        c = 45;
        assertTrue(Util.isEmoji(c));

        c = 48;
        assertTrue(Util.isEmoji(c));

        c = '~';
        assertFalse(Util.isEmoji(c));
    }

    @Test
    void isNumber() {
        char c = 17;
        assertTrue(Util.isNumber(c));
    }

    @Test
    void isNotNumber() {
        char c = 'x';
        assertFalse(Util.isNumber(c));
    }

    @Test
    void unescapeHtml() {
    }
}