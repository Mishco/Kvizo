package com.mishco.javaquiz.model;

import com.mishco.javaquiz.controller.QUESTION_THEME;

public class Parameter {
    private static Parameter ourInstance = new Parameter();

    private QUESTION_THEME questionTheme;
    private Integer numberOfQuestion;
    private Long duration;
    private Integer passingScore;

    public static void setInstance(QUESTION_THEME questionTheme, int numberOfQuestion, int duration, int passsingScore) {
        ourInstance.questionTheme = questionTheme;
        ourInstance.duration = (long) (duration * 60); // saved in seconds
        ourInstance.numberOfQuestion = numberOfQuestion;
        ourInstance.passingScore = passsingScore;
    }

    public static Parameter getInstance() {
        return ourInstance;
    }

    public QUESTION_THEME getQuestionTheme() {
        return questionTheme;
    }

    public Integer getNumberOfQuestion() {
        return numberOfQuestion;
    }

    public Long getDuration() {
        return duration;
    }

    public Integer getPassingScore() {
        return passingScore;
    }

    private Parameter() {
    }
}
