package com.mishco.javaquiz.model;

import java.util.List;


public class QuestionAndAnswer {

    private String title;
    private String question;
    private List<String> possibilities;
    private List<String> answers;


    public QuestionAndAnswer() {
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String titleQuestion) {
        this.title = titleQuestion;
    }

    public List<String> getPossibilities() {
        return possibilities;
    }

    public void setPossibilities(List<String> possibilities) {
        this.possibilities = possibilities;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }
}
