package com.mishco.javaquiz.model;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

public class Statistics {
    private int numberOfCorrectAnswer;
    private int numberOfFalseAnswer;
    private int passingScore;
    private List<QuestionAndAnswer> questionAndAnswerList;
    private BigDecimal percentRes;
    private MathContext mc;

    public Statistics(Parameter parameter) {
        numberOfCorrectAnswer = 0;
        numberOfFalseAnswer = 0;
        questionAndAnswerList = new ArrayList<>();
        if (parameter != null && parameter.getPassingScore() != null) {
            passingScore = parameter.getPassingScore();
        } else {
            passingScore = 0;
        }
        mc = new MathContext(2);
    }

    public void addCorrectAnswer() {
        this.numberOfCorrectAnswer++;
    }

    public void addFalseAnswer() {
        this.numberOfFalseAnswer++;
    }

    public int getNumberOfCorrectAnswer() {
        return numberOfCorrectAnswer;
    }

    public int getNumberOfFalseAnswer() {
        return numberOfFalseAnswer;
    }

    public void addQuestion(QuestionAndAnswer item) {
        questionAndAnswerList.add(item);
    }

    public BigDecimal getPercentRes() {
        return percentRes;
    }

    /**
     * Count percent of successfully answers
     *
     * @return true if percent is greater than passing value otherwise false
     */
    public boolean finishingResult() {
        int countOfAllQuestion = questionAndAnswerList.size();

        this.percentRes = (BigDecimal.valueOf(numberOfCorrectAnswer).multiply(BigDecimal.valueOf(100), mc)).divide(BigDecimal.valueOf(countOfAllQuestion), mc);
        return percentRes.compareTo(BigDecimal.valueOf(passingScore)) > 0;
    }
}
