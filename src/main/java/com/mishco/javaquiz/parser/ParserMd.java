package com.mishco.javaquiz.parser;

import com.mishco.javaquiz.model.QuestionAndAnswer;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ParserMd {
    private final static Logger LOGGER = Logger.getLogger(ParserMd.class.getName());

    private BlockParser blockParser = new BlockParser();

    public String parse(File file) {
        String parsed = null;
        BufferedReader br = null;
        try {
            List<String> lines = new ArrayList<>();
            br = new BufferedReader(new FileReader(file));
            String readed;
            while ((readed = br.readLine()) != null) {
                lines.add(readed);
            }
            parsed = blockParser.parser(lines);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Exception occur", e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return parsed;
    }


    public String parse(String source) {
        String parsed = null;
        try {
            String[] ls = source.split("\n");
            List<String> lines = Arrays.asList(ls);
            parsed = blockParser.parser(lines);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Exception occur", e);
        }
        return parsed;
    }

    public QuestionAndAnswer transformFromMarkdownToObj(String html) {
        QuestionAndAnswer qaa = new QuestionAndAnswer();

        Document document = Jsoup.parse(html);
        /* creating first order ArrayList */
        ArrayList<StringBuilder> textInsidePList = new ArrayList<>();
        Elements headings2 = document.select("h2");
        for (Element heading2 : headings2) {
            StringBuilder textInsideP = new StringBuilder();
            textInsideP.append(heading2); // delete this line to remove h2 content from array, this just for example
            parsingRecursion(heading2, textInsideP);
            textInsidePList.add(textInsideP);
        }

        /* iteraiting through ArrayList */
//        for (ArrayList<String> firstH2 : textInsidePList) {
//            //System.out.println("h2:");
//            for (String parsInsideH2 : firstH2) {
//                System.out.println(parsInsideH2);
//            }
//        }
        qaa.setQuestion(String.join("", textInsidePList.get(0)));
        qaa.setPossibilities(getStringsHtmlList(textInsidePList.get(1).toString()));
        qaa.setAnswers(getStringsHtmlList(textInsidePList.get(2).toString()));

        return qaa;
    }

    private List<String> getStringsHtmlList(String textInside) {
        Document doc = Jsoup.parse(textInside);
        List<String> poss = new ArrayList<>();
        for (int i = 0; i < doc.select("li").size(); i++) {
            poss.add(doc.select("li").get(i).text());
        }
        return poss;
    }

    private static void parsingRecursion(Element heading2, StringBuilder textInsideP) {
        Element nextPar = heading2.nextElementSibling();
        if (nextPar != null && !nextPar.is("h2")) {
            textInsideP.append(nextPar);
            parsingRecursion(nextPar, textInsideP);
        }
    }

}
