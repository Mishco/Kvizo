package com.mishco.javaquiz.parser;

public class Util {
    static boolean isEmoji(char c) {
        return c == 43 || c == 45 || c > 47 && c < 58 || c > 96 && c < 123;
    }

    static boolean isNumber(char c) {
        return c > 15 && c < 26;
    }

    static String unescapeHtml(String s) {
        char[] cs = s.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char c : cs) {
            if (c == '<') {
                sb.append("&lt;");
            } else if (c == '>') {
                sb.append("&gt;");
            } else if (c == '&') {
                sb.append("&amp;");
            } else if (c == '\"') {
                sb.append("&quot;");
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
