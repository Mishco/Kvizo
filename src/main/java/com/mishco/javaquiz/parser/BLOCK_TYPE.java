package com.mishco.javaquiz.parser;

public enum BLOCK_TYPE {
    P,
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
    HR,
    CODE,
    BLOCKQUOTES,
    UNORDERED_LIST,
    ORDERED_LIST,
    TASK_LIST,
    TABLE,
    TR,
    THEAD,
    TH,
    TBODY,

}
