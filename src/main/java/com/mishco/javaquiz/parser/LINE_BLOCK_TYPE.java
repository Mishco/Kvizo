package com.mishco.javaquiz.parser;

public enum LINE_BLOCK_TYPE {
    ITALIC,
    BOLD,
    IMAGE,
    LINK,
    INLINE_CODE,
    EMOJI,
    NORAML,
}
