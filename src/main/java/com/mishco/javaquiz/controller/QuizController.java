package com.mishco.javaquiz.controller;

import com.mishco.javaquiz.model.Parameter;
import com.mishco.javaquiz.model.QuestionAndAnswer;
import com.mishco.javaquiz.model.Statistics;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

public class QuizController implements Initializable {
    private final int SIZE_ANSWER = 1;
    private static final Logger LOGGER = Logger.getLogger(QuizController.class.getName());

    @FXML
    public WebView webView;

    @FXML
    public Pane bottomPane;

    @FXML
    public ListView<String> possibilityView;

    @FXML
    public Button submitButton;

    @FXML
    public Button startButton;

    @FXML
    public Button restartButton;

    @FXML
    public Button backButton;

    @FXML
    public Text timerText;

    private QuizMaker quizMaker;
    private QuestionAndAnswer currentQuestionAndAnswer;
    private Statistics statistics;
    private Timer timer;

    /**
     * Initialization method
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        quizMaker = new QuizMaker(Parameter.getInstance());
        statistics = new Statistics(Parameter.getInstance());

        possibilityView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        webView.getEngine().loadContent("<center><h2>Your Quiz is ready. <br>When you are able to start, click on start</h2></center>");
    }


    @FXML
    private void startQuiz() {
        submitButton.setDisable(false);
        startButton.setDisable(true);
        timerText.setText(secondsToString(Parameter.getInstance().getDuration()));
        long delay = 1000L;
        long period = 1000L;
        timer = new Timer();
        timer.scheduleAtFixedRate(task, delay, period);
        // TODO check if timer was canceled and reopen again

        currentQuestionAndAnswer = quizMaker.createQuestion();
        statistics.addQuestion(currentQuestionAndAnswer);
        webView.getEngine().loadContent(currentQuestionAndAnswer.getQuestion());
        possibilityView.setItems(FXCollections.observableArrayList(currentQuestionAndAnswer.getPossibilities()));
    }

    @FXML
    private void submitListener() {
        for (int i = 0; i < possibilityView.getSelectionModel().getSelectedItems().size(); i++) {
            for (int j = 0; j < currentQuestionAndAnswer.getAnswers().size(); j++) {
                String selected = possibilityView.getSelectionModel().getSelectedItems().get(i).substring(0, SIZE_ANSWER);
                if (currentQuestionAndAnswer.getAnswers().get(j).contains(selected)) {
                    // correct answer
                    statistics.addCorrectAnswer();
                } else {
                    // false answer
                    statistics.addFalseAnswer();
                }
            }
        }
        currentQuestionAndAnswer = quizMaker.createQuestion();
        if (currentQuestionAndAnswer == null) {
            showResult();
            return;
        }
        statistics.addQuestion(currentQuestionAndAnswer);
        webView.getEngine().loadContent(currentQuestionAndAnswer.getQuestion());
        possibilityView.setItems(FXCollections.observableArrayList(currentQuestionAndAnswer.getPossibilities()));
    }

    @FXML
    private void onPossibilityViewClickListener(MouseEvent event) {
        if (event.getClickCount() == 2) {
            submitListener();
        }
    }

    private void showResult() {
        submitButton.setVisible(false);
        startButton.setVisible(false);

        restartButton.setVisible(true);
        backButton.setVisible(true);

        possibilityView.getSelectionModel().clearSelection();
        possibilityView.setItems(null);
        if (statistics.finishingResult()) {
            webView.getEngine().loadContent("<h1>Results</h1><br>Correct answer: " + statistics.getNumberOfCorrectAnswer() + " <br> " +
                    "Number of false answer: " + statistics.getNumberOfFalseAnswer()
                    + " <br> " + " Percent: " + statistics.getPercentRes().toPlainString() + " %");
        } else {
            webView.getEngine().loadContent("<h1>Results</h1><br>FAILED! Your " + " percent: " + statistics.getPercentRes().toPlainString() + " %");
        }
    }


    @FXML
    private void back() throws IOException {
        Stage secondStage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/views/intro.fxml"));
        secondStage.setScene(new Scene(root));
        secondStage.initStyle(StageStyle.UNDECORATED);
        secondStage.show();
        Stage stage = (Stage) backButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void restart() {
        LOGGER.info("Restart Quiz with same parameters");

        submitButton.setVisible(true);
        submitButton.setDisable(true);

        startButton.setVisible(true);
        startButton.setDisable(false);

        restartButton.setVisible(false);
        backButton.setVisible(false);

        initialize(null, null);
    }

    @FXML
    private void close(MouseEvent event) {
        System.exit(0);
    }

    @FXML
    private void minimize(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }

    private String secondsToString(Long pTime) {
        if (pTime <= 0) {
            timer.purge();
            return "00:00";
        }
        return String.format("%02d:%02d", pTime / 60, pTime % 60);
    }

    private String secondsMinusOne(String inputTime) {
        String[] units = inputTime.split(":");
        int minutes = Integer.parseInt(units[0]);
        int seconds = Integer.parseInt(units[1]);
        long duration = 60 * minutes + seconds - 1;
        return secondsToString(duration);
    }

    TimerTask task = new TimerTask() {
        public void run() {
            String oldText = timerText.getText(); // for example "60:00"
            timerText.setText(secondsMinusOne(oldText));
        }
    };
}
