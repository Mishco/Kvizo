package com.mishco.javaquiz.controller;

public enum QUESTION_THEME {
    OCA,
    ECONOMY,
    JAVA,
    DEFAULT
}
