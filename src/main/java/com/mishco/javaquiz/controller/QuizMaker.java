package com.mishco.javaquiz.controller;

import com.mishco.javaquiz.model.Parameter;
import com.mishco.javaquiz.model.QuestionAndAnswer;
import com.mishco.javaquiz.parser.ParserMd;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class QuizMaker {
    private static final Logger LOGGER = Logger.getLogger(QuizMaker.class.getName());
    private int currentIndex;
    private final ParserMd parserMd;
    private List<File> listQuestions;


    QuizMaker(Parameter parameter) {
        if (parameter != null) {
            if (parameter.getQuestionTheme().equals(QUESTION_THEME.DEFAULT)) {
                String firstNameOfFile = "/questions/01.md";
                this.listQuestions = getCollectFiles(getStringDirFromResources(firstNameOfFile));
            }
            if (parameter.getQuestionTheme().equals(QUESTION_THEME.OCA)) {
                String firstNameOfFile = "/questions/oca/01.md";
                this.listQuestions = getCollectFiles(getStringDirFromResources(firstNameOfFile));
            }
            if (parameter.getQuestionTheme().equals(QUESTION_THEME.ECONOMY)) {
                String firstNameOfFile = "/questions/economy/01.md";
                this.listQuestions = getCollectFiles(getStringDirFromResources(firstNameOfFile));
            }
        }
        this.parserMd = new ParserMd();
        this.currentIndex = 0;
    }

    private List<File> getCollectFiles(String dir) {
        try {
            return Files.list(Paths.get(dir))
                    .map(Path::toFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Exception occur", e);
            return new ArrayList<>();
        } finally {
            LOGGER.info("Success added files for Quiz");
        }
    }

    /**
     * Get first file from dir
     *
     * @param firstNameOfFile name of first file in directory (in can be any on one file from dir)
     * @return name of directory
     */
    private String getStringDirFromResources(String firstNameOfFile) {
        File firstFile = new File(Objects.requireNonNull(getClass().getResource(firstNameOfFile)).getFile());
        return Paths.get(firstFile.getParentFile().toURI()).toString();
    }

    QuestionAndAnswer createQuestion() {
        if (listQuestions != null) {
            if (currentIndex >= listQuestions.size()) {
                return null;
            } else {
                return parserMd.transformFromMarkdownToObj(parserMd.parse(getRandomFile()));
            }
        }
        return null;
    }

    /**
     * Select Random File Without Repetitions List Element
     *
     * @return File with content for quiz
     */
    private File getRandomFile() {
        Random rand = new SecureRandom();
        int randomIndex = rand.nextInt(listQuestions.size());
        File result = listQuestions.get(randomIndex);
        listQuestions.remove(randomIndex);
        return result;
    }
}
