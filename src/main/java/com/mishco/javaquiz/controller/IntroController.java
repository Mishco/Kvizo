package com.mishco.javaquiz.controller;

import com.mishco.javaquiz.model.Parameter;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class IntroController implements Initializable {
    private static final Logger LOGGER = Logger.getLogger(IntroController.class.getName());

    @FXML
    public Button startQuizButton;

    @FXML
    public ChoiceBox<QUESTION_THEME> topicChoiceBox;

    @FXML
    public ChoiceBox<Integer> numberOfQuestionChoiceBox;

    @FXML
    public ChoiceBox<Integer> durationChoiceBox;

    @FXML
    public ChoiceBox<Integer> passingScoreBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (topicChoiceBox != null) {
            topicChoiceBox.setItems(FXCollections.observableArrayList(QUESTION_THEME.values()));
            topicChoiceBox.getSelectionModel().select(0);
        }
        if (numberOfQuestionChoiceBox != null) {
            numberOfQuestionChoiceBox.setItems(FXCollections.observableArrayList(10, 15, 20, 25, 30, 40, 50, 70, 80, 100));
            numberOfQuestionChoiceBox.getSelectionModel().select(0);
        }
        if (durationChoiceBox != null) {
            durationChoiceBox.setItems(FXCollections.observableArrayList(60, 120, 150, 200));
            durationChoiceBox.getSelectionModel().select(0);
        }
        if (passingScoreBox != null) {
            passingScoreBox.setItems(FXCollections.observableArrayList(65, 75, 85, 90));
            passingScoreBox.getSelectionModel().select(0);
        }
    }

    /**
     * Take parameters and open new window
     *
     * @throws IOException if .frml does not found
     */
    @FXML
    private void startQuiz() throws IOException {
        Parameter.setInstance(topicChoiceBox.getSelectionModel().getSelectedItem(),
                numberOfQuestionChoiceBox.getSelectionModel().getSelectedItem(),
                durationChoiceBox.getSelectionModel().getSelectedItem(),
                passingScoreBox.getSelectionModel().getSelectedItem());

        Stage secondStage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/views/quiz.fxml"));
        secondStage.setTitle("Explore recipes");
        secondStage.setScene(new Scene(root));
        secondStage.initStyle(StageStyle.UNDECORATED);
        secondStage.show();
        Stage stage = (Stage) startQuizButton.getScene().getWindow();
        stage.close();

        LOGGER.info("Starting quiz");
    }

    @FXML
    private void close(MouseEvent event) {
        System.exit(0);
    }

    @FXML
    private void minimize(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }
}
