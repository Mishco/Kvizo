# Economy 1 

## Question 3 

An increase from 5% to 8% in the interest rates charged by banks would most likely encourage:
   
## Possibilities

* A. Businesses to invest.
* B. People to purchase housing.
* C. People to save money.
* D. Don't Know.

## Right answer

* C


